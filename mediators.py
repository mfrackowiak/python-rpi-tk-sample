import contextlib

from vadain.rpi.client import (settings, logger, rest, errors, models)
from vadain.rpi.client.i18n import _

log = logger.logging.getLogger(__name__)


@contextlib.contextmanager
def handle_basic_errors(ctrl):
    log.debug(u'\n[handle basic errors]\n')
    try:
        yield

    except errors.PrinterNotFoundError as e:
        ctrl.printer_exception = e
        ctrl.show_printer_not_found()

    except errors.ServerError:
        ctrl.msg_object.set(400, _(u'Server Error'), _(u'server internal error [500]'))
        ctrl.show_msg()

    except errors.ServerConnectionError:
        ctrl.show_connection_frame()

    except (errors.BadLoginCredentials, errors.NotAcceptableDataError) as e:
        ctrl.msg_object.set(400, '[ Exception ]', e.message)
        ctrl.show_msg()

    except errors.InvalidResponseError:
        log.debug(u'[handle_empty_response][except]')
        ctrl.set_basic_form()

    except errors.TimeoutError:
        log.debug(u'[handle_timeout][except]')
        ctrl.msg_object.set(400, 'Timeout', 'no response!')
        ctrl.show_msg()

    except errors.BillingPrintedError:
        log.debug(u'[handle_billing_printed_error][except]')
        ctrl.set_basic_order_form()
        ctrl.error_to_confirm = (
            u'BILLING PRINTED',
            u'ORDER STATUS COULD NOT BE UPDATED'
        )
        ctrl.show_error_confirmation()


class ViewMediator(object):
    def __init__(self, ctrl):
        self.ctrl = ctrl
        self.rest = rest.RequestController()

    def run_form_action(self):
        """ [ check server connection before showing form ] """
        log.debug(u'[run_form][server_on][%s]')

        with handle_basic_errors(self.ctrl):
            self.ctrl.data['check_server_response'] = self.rest.get_device_type()
            if settings.TEST_PRINTER_CONNECTION == True:
                self.ctrl.printing.check_if_printer_online()
            self.ctrl.show_frame(self.ctrl.basic_form)

    def printrfid_login_handler(self):
        log.debug(u'[print rfid client login action]')

        with handle_basic_errors(self.ctrl):
            try:
                self.ctrl.data['login'] = self.rest.login_action(
                    self.ctrl.card_code,  # get code from entry field
                    settings.DEVICE_NAME
                )

                self.ctrl.set_basic_order_form()
                self.ctrl.run_form()

            except (errors.BadLoginCredentials, errors.NotAcceptableDataError) as e:
                self.ctrl.set_basic_form()
                self.ctrl.msg_object.set(400, '[ Exception ]', e.message)
                self.ctrl.show_msg()

    def logout_handler(self):

        with handle_basic_errors(self.ctrl):
            data = self.rest.logout_action(self.ctrl.card_code)
            self.ctrl.data['logout'] = data

            # success : set basic frame and show msg
            self.ctrl.set_basic_form()
            self.ctrl.msg_object.set(
                data.status_code,
                data.employee_name,
                data.result_msg
            )
            self.ctrl.show_msg()

    def order_tag_check_handler(self):
        """ [ print rfid client - order tag check event handler] """
        log.debug(u'[order_tag_check request]')

        # clear object's
        self.ctrl.data['order_tag'] = models.OrderTagResponse({})
        self.ctrl.item_count = models.OrderTagAndHangerCount()

        with handle_basic_errors(self.ctrl):
            try:
                data = self.rest.get_order_tag_data(
                    self.ctrl.card_code,
                    self.ctrl.order_code,
                    settings.DEVICE_NAME,
                )
                self.ctrl.data['order_tag'] = data

                # success
                if data.tags_connected == 0:
                    log.debug(u'[no tags connected][connect and print first tag]')
                    self.print_tag_handler()
                else:
                    log.debug(u'[tags connected][tag edition screen]')
                    self.ctrl.order_tag_ctrl.show_edit_order_tag_screen(data)

            except (errors.BadLoginCredentials, errors.NotAcceptableDataError) as e:
                self.ctrl.msg_object.set(400, 'Exception', e.message)
                self.ctrl.show_msg()

    def save_changes_to_db_success_scenario(self, response):
        log.debug(u'[save_changes_to_db_handler][success]')
        if self.ctrl.item_count.tags > response.tags_connected:
            log.debug(u'[save_changes_to_db_handler][print next tag]')
            self.print_tag_handler()
        else:
            log.debug(u'[save_changes_to_db_handler][show msg]')
            self.ctrl.item_count.tags = 0
            self.ctrl.set_basic_order_form()

            if response.result_code == 'status_300_not_set':
                self.ctrl.msg_object.set(
                    550, 'STATUS WARNING', 'NOT POSSIBLE TO SET STATUS 300!'
                )
            else:
                self.ctrl.printing.set_success_msg()

            self.ctrl.show_msg()

    def save_hangers_to_db_handler(self):
        log.debug(u'[save_hangers_to_db_handler]')

        with handle_basic_errors(self.ctrl):
            self.ctrl.data['order_tag'] = self.rest.update_order_tag_data(
                card_code=self.ctrl.card_code,
                order_number=self.ctrl.data['order_tag'].order_number,
                device_name=settings.DEVICE_NAME,
                hanger_count=int(self.ctrl.item_count.hangers),
            )
            self.ctrl.flags['enter_button_pressed'] = False

            self.ctrl.set_basic_order_form()

            if self.ctrl.data['order_tag'].result_code == 'status_300_not_set':
                self.ctrl.msg_object.set(
                    550, 'STATUS WARNING', 'NOT POSSIBLE TO SET STATUS 300!'
                )
            elif self.ctrl.item_count.hanger_amount > 0:
                self.ctrl.msg_object.set(
                    1001, _('HANGER ADDED'),
                    u'%s %s.' % (unicode(self.ctrl.item_count.hanger_amount), _(u'hanger connected to order'))
                )
            elif self.ctrl.item_count.hanger_amount < 0:
                self.ctrl.msg_object.set(
                    1001, _('HANGER REMOVED'),
                    u'%s %s!' % (str(abs(self.ctrl.item_count.hanger_amount)), _(u'hanger removed from order'))
                )

            self.ctrl.show_msg()

    def save_changes_to_db_handler(self):
        log.debug(u'[save_changes_to_db_handler]')

        with handle_basic_errors(self.ctrl):
            self.ctrl.data['order_tag'] = self.rest.update_order_tag_data(
                card_code=self.ctrl.card_code,
                order_number=self.ctrl.data['order_tag'].order_number,
                device_name=settings.DEVICE_NAME,
                hanger_count=int(self.ctrl.item_count.hangers),
                printed_tag=self.ctrl.data['order_tag'].tag
            )
            self.ctrl.flags['enter_button_pressed'] = False

            # success:
            self.save_changes_to_db_success_scenario(self.ctrl.data['order_tag'])

    def print_tag_handler(self):
        with handle_basic_errors(self.ctrl):
            # GET new order tag
            data = self.rest.get_new_order_tag()
            self.ctrl.data['order_tag'].tag = data.tag

            # success  (print received order tag)
            self.ctrl.print_tag(self.ctrl.data['order_tag'])

    def entrance_login_handler(self):
        """ [ main client - login event on entrance device handler ] """
        log.debug(u'[entrance login request]')

        with handle_basic_errors(self.ctrl):
            data = self.rest.login_action(
                self.ctrl.card_code,
                settings.DEVICE_NAME
            )
            self.ctrl.msg_object.set(
                data.status_code,
                data.employee_name,
                data.result_msg
            )
            self.ctrl.data['login'] = data
            self.ctrl.show_msg()

    def exit_logout_handler(self):
        """ [ main client - exit logout handler ] """
        log.debug(u'[exit logout request]')

        with handle_basic_errors(self.ctrl):
            data = self.rest.logout_action(
                self.ctrl.card_code,
                settings.DEVICE_NAME
            )
            self.ctrl.msg_object.set(
                data.status_code,
                data.employee_name,
                data.result_msg
            )
            self.ctrl.data['logout'] = data
            self.ctrl.show_msg()

    def machine_login_handler(self):
        """ [ main client - machine type device login event ] """
        log.debug(u'[machine login request]')

        with handle_basic_errors(self.ctrl):
            data = self.rest.login_action(
                self.ctrl.card_code,
                settings.DEVICE_NAME
            )
            self.ctrl.data['login'] = data
            self.ctrl.msg_object.set(
                data.status_code,
                data.employee_name,
                data.result_msg
            )
            self.ctrl.show_even_selection()

    def register_order_handler(self):
        """ [ main client - order registration on machine event ] """
        log.debug(u'[register_order request]')

        with handle_basic_errors(self.ctrl):
            try:
                data = self.rest.order_registration_action(
                    self.ctrl.order_code,
                    self.ctrl.card_code,
                    self.ctrl.event
                )
                self.ctrl.order_registration_ctrl.load_data_into_frame(data)
                self.ctrl.show_msg()

            except errors.NotAcceptableDataError as e:
                self.ctrl.set_basic_order_form()
                self.ctrl.msg_object.set(400, '[ Exception ]', e.message)
                self.ctrl.show_msg()

            except errors.InvalidResponseError as e:
                self.ctrl.set_basic_order_form()
                self.ctrl.msg_object.set(400, '[ Exception ]', e.message)

    def close_order_handler(self):
        """ [ main client - close order on machine event ] """
        log.debug(u'[close order request]')

        try:
            with handle_basic_errors(self.ctrl):
                data = self.rest.close_record_action(
                    self.ctrl.order_code,
                    self.ctrl.card_code,
                    self.ctrl.event
                )
                self.ctrl.msg_object.set(
                    data.status_code,
                    data.result_msg,
                    data.result_msg
                )
                self.ctrl.data['close_order'] = data
                self.ctrl.show_msg()
        except errors.InvalidResponseError:
            self.ctrl.set_basic_order_form()
