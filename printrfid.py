import time
import struct

try:
    import RPi.GPIO as GPIO
except:
    from vadain.rpi.client import mockgpio as GPIO
from vadain.rpi.toshiba.response_codes import tcl_response
from vadain.rpi.toshiba.rfid import RFIDLabelPrinter
from vadain.rpi.client import (settings, logger, utils)
from vadain.rpi.client.i18n import _
from vadain.rpi.client.common_stuff import (
    Connection, InfoBtnScreen, StartFrame, ScanCodeFrame, ScanOrderCodeFrame,
    MsgFrame, Master, WaitFrame, RPI, RestartBtnScreen, ShutdownBtnScreen
)
from vadain.rpi.client.buttons import *
from vadain.rpi.client.labels import *
from vadain.rpi.client.events import *
from vadain.rpi.client.models import OrderTagAndHangerCount
from vadain.rpi.client.errors import PrinterNotFoundError

log = logger.logging.getLogger(__name__)


class OrderTagAndHangerEditionCtrl(object):
    def __init__(self, ctrl):
        self.ctrl = ctrl

    def print_tag_and_show_msg(self, response_obj):
        log.debug(u'[RPi][ScanOrderCode][process][new_tag_connected]')
        self.ctrl.hide_enter_btn_show_clock()
        self.ctrl.print_tag(response_obj)

    def show_edit_order_tag_screen(self, response_obj):
        log.debug(u'[RPi][ScanOrderCode][process][order_has_tags_connected]')
        self.ctrl.update_item_counts(response_obj)
        self.ctrl.draw_item_count()

    def order_tag_edited(self):
        """
        order tag edit event:  (enter press)
          1. print and save connected tags
          2. save hanger count
        """
        log.debug(u'[order_tag_edited]')
        if self.ctrl.item_count.tags_added > 0:
            log.debug(u'[order_tag_edited][print tag request]')
            self.ctrl.view_mediator.print_tag_handler()
        elif self.ctrl.item_count.hanger_amount != 0:
            log.debug(u'[order_tag_edited][hangers edited]')
            self.ctrl.view_mediator.save_hangers_to_db_handler()
        elif self.ctrl.data['order_tag'].result_code == 'status_300_not_set':
            self.ctrl.msg_object.set(
                550, 'STATUS WARNING', 'NOT POSSIBLE TO SET STATUS 300!'
            )
            self.ctrl.show_msg()
        else:
            log.debug(u'[order_tag_edited][nothing changed]')
            self.ctrl.msg_object.set(0, _(u'NOTHING TO DO'), _(u'No changes detected!'))
            self.ctrl.show_msg()


class ToshibaPrintCtrl(object):
    """ Class for handling printing on toshiba """

    def __init__(self, ctrl):
        self.ctrl = ctrl
        self.order_tag_data = None
        self.printer_online = False

    def check_if_printer_online(self):
        if self.printer_online == False:
            log.debug(u'[validate printer connection]')
            self.printer_online = True
            if self.ctrl.printer.online() == False:
                self.printer_online = False
                raise PrinterNotFoundError(u'[printer not found]')

    def unpack_response(self, binary_response):
        unpacked = struct.unpack(
            '13B',  # binary response length
            binary_response
        )
        response = ''
        for elm in unpacked:
            relm = elm - 48  # 48 in unpacked binary response string = 0
            # to conserve tcl_response mapping
            # only above 0 is used in positions 2:4 and 5:9
            response += relm >= 0 and str(relm) or '-'
        return response

    def get_printer_state(self):
        """
        Toshiba printer possible states:
         - idle
         - busy
         - offline
         - exception
         - simulate (only for debug)
        """
        if settings.SIMULATE_PRINT:
            return 'simulate'
        try:
            result, msg, binary_response = self.ctrl.printer.test_connection()
            response = self.unpack_response(binary_response)
            tcl_code = tcl_response[response[2:4]]
            if tcl_code == 'Idle' and response[5:9] == '0000':
                return 'idle'
            elif tcl_code not in [
                'in operation',
                'waiting for striping'
            ]:
                self.ctrl.printer.exception = tcl_code
                return 'exception'
            return 'busy'
        except BaseException:
            return 'offline'

    def print_tag_on_toshiba(self, order_tag_data):
        if order_tag_data != None:
            self.order_tag_data = order_tag_data
        log.debug(u'[print_tag_on_toshiba]')
        printer_state = self.get_printer_state()

        if printer_state == 'offline':
            log.debug(u'[print_tag_on_toshiba][printer is offline]')
            self.ctrl.show_frame(PrinterOffline)

        elif printer_state == 'busy':
            log.debug(u'[print_tag_on_toshiba][printer is busy]')
            self.ctrl.show_frame(PrinterBusy)

        elif printer_state == 'idle':
            log.debug(u'[print_tag_on_toshiba][printer is idle][start print]')
            try:
                result, msg, response = self.ctrl.printer.print_label(
                    rfidtag=self.order_tag_data.tag,
                    order_number=int(self.order_tag_data.order_number),
                    customer_name=self.order_tag_data.debtor_cname,
                    customer_city=self.order_tag_data.commission_name
                )
                if result != False:
                    self.ctrl.flags['wait_for_peel_off'] = True
                    self.ctrl.show_frame(PeelOff)
                else:
                    self.ctrl.show_frame(PrinterOffline)
            except BaseException as e:
                log.debug(u'[Print tag][Exception] E:%s' % e)
                self.ctrl.show_frame(PrinterError)

        elif printer_state == 'exception':
            log.debug(u'[print_tag_on_toshiba][printer exception]')
            self.ctrl.show_frame(PrinterError)

        elif printer_state == 'simulate':
            log.debug(u'[print_tag_on_toshiba][simulate]')
            self.ctrl.flags['wait_for_peel_off'] = True
            self.ctrl.show_frame(PeelOff)

    def set_success_msg(self):
        try:
            self.ctrl.msg_object.set(
                1000,
                _(u'PRINTED LABEL'),
                _(u'tags printed & connected\n %s') % (
                    self.ctrl.data['order_tag'].debtor_cname,
                )
            )
            log.debug(u'[set_success_msg][successfully]')
        except BaseException as e:
            log.debug(u'[set_success_msg][exception:%s]' % e)


class PrintRFID(RPI):
    """ RPI Print RFID TAG CLIENT """

    def __init__(self, *args, **kwargs):
        super(PrintRFID, self).__init__(*args, **kwargs)
        log.debug(u'[PrintRFID][init]')
        self.render_container()
        self.item_count = OrderTagAndHangerCount()
        self.order = None
        self.error_to_confirm = None
        self.printer_exception = ''
        self.tag_queue = []

        self.order_tag_ctrl = OrderTagAndHangerEditionCtrl(self)
        self.printing = ToshibaPrintCtrl(self)
        self.load_frames()
        self.frames[StartFrame].tkraise()
        self.update_clock()
        self.set_basic_form()
        self.currently_open_frame = self.basic_form
        self.printer = RFIDLabelPrinter(settings.PRINTER_URL, int(settings.PRINTER_PORT))

    def print_tag(self, response_obj):
        self.printing.print_tag_on_toshiba(response_obj)

    def render_container(self):
        # main container
        self.geometry('{}x{}'.format(320, 240))
        self.gui_objects['container'] = tk.Frame(self)
        self.gui_objects['container'].pack_propagate(0)
        self.gui_objects['container'].pack(side='top', fill='both', expand=True)
        self.gui_objects['container'].grid_rowconfigure(0, weight=1)
        self.gui_objects['container'].grid_columnconfigure(0, weight=1)

        # bottom label
        self.gui_objects['bottom_time_label'] = TimeLabel(self)
        self.gui_objects['bottom_time_label'].pack(side='top', fill='both', expand=True)
        self.gui_objects['order_tag_enter_button'] = EnterButtonBottom(self)

    def set_basic_form(self):
        self.basic_form = ScanCardCode

    def set_basic_order_form(self):
        self.basic_form = ScanOrderCode

    def draw_item_count(self):
        self.show_frame(EditTag)

    def show_peel_off(self):
        self.show_frame(PeelOff)

    def show_error_confirmation(self):
        self.show_frame(ErrorConfirmation)

    def load_frames(self):
        for current_frame in (
                StartFrame,
                ScanCardCode,
                ScanOrderCode,
                EditTag,
                MsgFrame,
                Connection,
                InfoBtnScreen,
                RestartBtnScreen,
                ShutdownBtnScreen,
                PrinterNotFound,
                PrinterError,
                PeelOff,
                PrinterBusy,
                WaitFrame,
                PrinterOffline,
                ErrorConfirmation,
        ):
            frame = current_frame(self.gui_objects['container'], self)
            self.frames[current_frame] = frame
            frame.grid(row=0, column=0, sticky='nsew')

    def show_printer_not_found(self):
        self.show_frame(PrinterNotFound)

    def run_form(self):
        self.view_mediator.run_form_action()

    def hide_enter_btn_show_clock(self):
        self.gui_objects['bottom_time_label'].pack(side='top', fill='both', expand=True)
        self.gui_objects['order_tag_enter_button'].pack_forget()
        self.flags['confirming_order_tag_edit'] = False
        self.after(1000, self.update_clock)

    def update_clock(self):
        if not self.flags['confirming_order_tag_edit']:
            now = time.strftime('%Y-%m-%d    %H:%M:%S')
            self.gui_objects['bottom_time_label'].configure(text=now)
            self.after(1000, self.update_clock)
        else:
            self.gui_objects['bottom_time_label'].pack_forget()
            self.gui_objects['order_tag_enter_button'].pack()

    def handle_event(self, event):
        """ button event processor """
        log.debug(u'[Process][event:%s]' % event)
        if event == ORDER_TAG_EDIT_EVENT:  # Order tag edition complete signal
            self.hide_enter_btn_show_clock()
            self.flags['enter_button_pressed'] = True
            self.async(self.order_tag_ctrl.order_tag_edited)

    def update_item_counts(self, response_obj):
        self.item_count.tags = response_obj.tags_connected

        if isinstance(response_obj.hangers_connected, int):
            self.item_count.hangers = response_obj.hangers_connected
        else:
            self.item_count.hangers = 0
        self.item_count.tags_start_count = self.item_count.tags
        self.item_count.hangers_start_count = self.item_count.hangers


class PrinterOffline(Master):
    """ [ printer offline screen ] """

    def __init__(self, master, ctrl):
        super(PrinterOffline, self).__init__(master, ctrl)
        self['bg'] = 'darkred'
        label = DarkRedLabel(self, set_text=u'[ %s ]' % _(u'PRINTER'))
        label.pack(pady=5, padx=0)
        self.exception_label = DarkRedLabel(self, set_text=u'[ %s ]' % _(u'is OFFLINE'))
        self.exception_label.pack(pady=5, padx=0)
        self.button_recommence = RecommenceButton(self)
        self.button_recommence.pack(pady=10, padx=6)
        self.button_cancel = PrintCancelButton(self)
        self.button_cancel.pack(pady=5, padx=6)

    def handle_event(self, event):
        log.debug(u'[PrinterOffline][Process][event:%s]' % event)
        self.ctrl.flags['block_input'] = False
        if event == RECOMMENCE_EVENT:
            self.ctrl.print_tag(None)
        elif event == PRINT_CANCEL_EVENT:
            self.ctrl.run_form()

    def reload(self):
        self.ctrl.flags['block_input'] = True


class PrinterError(Master):
    """ is the tag printer correct question: Yes/No """

    def __init__(self, master, ctrl):
        super(PrinterError, self).__init__(master, ctrl)
        self['bg'] = 'darkred'
        self.exception_label = DarkRedLabel(self, set_text=u'[ %s ]' % _(u'PRINTER'))
        self.exception_label.pack(pady=5, padx=0)
        title = DarkRedLabel(self)
        title.pack(pady=0, padx=0)
        title.config(text=_(u'is the tag printed correctly?'))
        self.button_yes = YesButton(self)
        self.button_yes.pack(pady=5, padx=6)
        self.button_no = NoButton(self)
        self.button_no.pack(pady=5, padx=6)

    def handle_event(self, event):
        log.debug(u'[PrinterError][Process][event:%s]' % event)
        self.ctrl.flags['block_input'] = False
        if event == PRINTED_CORRECT_EVENT:
            self.ctrl.run_form()
        elif event == PRINTING_FAILED_EVENT:
            log.debug(u'[PRINTING_FAILED_EVENT][try print again]')
            self.ctrl.print_tag(self.ctrl.data['order_tag'])

    def reload(self):
        exception = utils.crop_str(self.ctrl.printer_exception, 33)
        self.exception_label.config(text=_(u'[ error: %s ]' % exception))
        self.ctrl.flags['block_input'] = True


class PrinterInfo(Master):
    """ [ print info form's master form ] """

    def __init__(self, master, ctrl):
        super(PrinterInfo, self).__init__(master, ctrl)
        self['bg'] = 'darkorange'
        label = OrangeLabel(self, set_text=u'[ %s ]' % _(u'PRINTER'))
        label.pack(pady=0, padx=0)


class PrinterNotFound(PrinterInfo):
    """ [ print connection error screen ] """

    def __init__(self, master, ctrl):
        super(PrinterNotFound, self).__init__(master, ctrl)
        self.exception_label = PrinterExceptionLabel(self)
        self.exception_label.pack(pady=0, padx=0)
        self.connection_counter_label = WarningConnectionCounter(self)
        self.connection_counter_label.pack(pady=0, padx=0)

    def reload(self):
        self.start_counter(self.connection_counter_label, 5, u'reconnect\n in \n%s s')
        self.exception_label.config(text=self.ctrl.printer_exception)


class PrinterBusy(Master):
    def __init__(self, master, ctrl):
        super(PrinterBusy, self).__init__(master, ctrl)
        self['bg'] = 'darkblue'
        label = DarkBlueLabel(self, set_text=u'[ %s ]' % _(u'PRINTER'))
        label.pack(pady=0, padx=0)
        title = PrinterExceptionLabel(self)
        title.pack(pady=0, padx=0)
        title.config(text=_(u'IS BUSY..'))
        self.counter_label = RPILabel(self)
        self.counter_label.pack(pady=0, padx=0)
        self.counter_label.config(bg=self['bg'])

        self.busy_counter = 0

    def printer_busy_re_check(self):
        self.ctrl.basic_form = PrinterBusy
        self.start_counter(
            self.counter_label,
            settings.BUSY_RECHECK_PERIOD,  # printer busy check period
            u'%s s (try:%s)' % (u'%s', self.busy_counter)
        )

    def reload(self):
        self.ctrl.flags['block_input'] = True
        log.debug(u'[PrinterBusy][reload]')
        self.busy_counter += 1
        if self.ctrl.printing.get_printer_state() != 'idle':
            log.debug(u'[PrinterBusy][reload][printer not idle][trigger re-check]')

            if self.busy_counter >= settings.BUSY_RECHECK_LIMIT:
                self.busy_counter = 0
                self.ctrl.show_frame(PrinterOffline)
            else:
                log.debug(u'[PrinterBusy][reload][wait for idle!][re-heck]')
                self.printer_busy_re_check()
        else:
            log.debug(u'[PrinterBusy][reload][printer not idle][continue tag printing]')
            self.ctrl.print_tag(None)
            self.busy_counter = 0
            self.ctrl.flags['block_input'] = False


class PeelOff(Master):
    def __init__(self, master, ctrl):
        super(PeelOff, self).__init__(master, ctrl)
        self['bg'] = 'darkblue'
        label = DarkBlueLabel(self, set_text=u'[ %s ]' % _(u'PRINTER'))
        label.pack(pady=0, padx=0)
        title = PeelOffLabel(self)
        title.pack(pady=0, padx=0)
        title.config(text=_(u'PEEL OFF'))
        self.counter_label = RPILabel(self)
        self.counter_label.pack(pady=0, padx=0)
        self.counter_label.config(bg=self['bg'])
        self.peel_off_counter = 0

    def peel_off_re_check(self):
        self.ctrl.basic_form = PeelOff
        self.start_counter(
            self.counter_label,
            settings.PEEL_OFF_RECHECK_PERIOD,  # PEEL OFF re check period
            u'%s s (try:%s)' % (u'%s', self.peel_off_counter)
        )

    def reload(self):
        log.debug(u'[PeelOff][reload]')
        if self.ctrl.flags['wait_for_peel_off']:
            log.debug(u'[PeelOff][reload][wait for peel off!]')
            printer_state = self.ctrl.printing.get_printer_state()
            if printer_state == 'idle' or printer_state == 'simulate':  # tag is peeled off.
                log.debug(u'\n\- Peeled Off -\n')
                self.ctrl.flags['wait_for_peel_off'] = False
                self.peel_off_counter = 0
                self.ctrl.view_mediator.save_changes_to_db_handler()
            else:
                if self.peel_off_counter == settings.PEEL_OFF_RECHECK_LIMIT:
                    log.debug(u'[PeelOff][reload][PRINTER_PEEL_OFF_RE_CHECK_LIMIT][show recommence]')
                    self.peel_off_counter = 0
                    self.ctrl.show_frame(PrinterOffline)
                else:
                    log.debug(u'[PeelOff][reload][wait for peel off!][re-heck]')
                    self.peel_off_re_check()
            self.peel_off_counter += 1


class ErrorConfirmation(Master):
    """ One button error confirm screen """

    def __init__(self, master, ctrl):
        super(ErrorConfirmation, self).__init__(master, ctrl)
        self['bg'] = 'darkred'
        self.exception_label = DarkRedLabel(self, set_text=u'[ %s ]' % _(u'Error:'))
        self.exception_label.pack(pady=5, padx=0)
        self.title = DarkRedLabel(self)
        self.title.pack(pady=0, padx=0)
        self.title.config(text=_(u'exception info'))
        self.button_confirm = ConfirmButton(self)
        self.button_confirm.pack(pady=5, padx=6)

    def handle_event(self, event):
        log.debug(u'[PrinterError][Process][event:%s]' % event)
        self.ctrl.flags['block_input'] = False
        if event == CONFIRM_EVENT:
            self.ctrl.run_form()

    def reload(self):
        if self.ctrl.error_to_confirm:
            error = utils.crop_str(self.ctrl.error_to_confirm[0], 33)
            description = utils.crop_str(self.ctrl.error_to_confirm[1], 60)
            self.exception_label.config(text=_(u'[ Error: %s ]' % error))
            self.title.config(text=_(u'%s\n order: %s' % (
                description, self.ctrl.order_code
            )))
        self.ctrl.flags['block_input'] = True


class ScanCardCode(ScanCodeFrame):
    def __init__(self, master, ctrl):
        super(ScanCardCode, self).__init__(master, ctrl)
        self.header.config(text=u'[ %s ]' % _(u'Print RFID'))
        self.title.config(text=_(u'SCAN CARD CODE'))
        self.ctrl.wm_title(u'[ %s ] %s' % (settings.DEVICE_NAME, _(u'SCAN CARD')))
        self.wait_bool = True

    def process(self):  # only possible action - scan card code!
        log.debug(u'[RPi][ScanCardCode][code scanned!]')
        self.ctrl.card_code = self.code_var.get()  # set card code to temp var
        self.ctrl.async(self.ctrl.view_mediator.printrfid_login_handler)

    def reload(self):
        self.code_var.set('')
        self.code_field.focus()


class ScanOrderCode(ScanOrderCodeFrame):
    def __init__(self, master, ctrl):
        super(ScanOrderCode, self).__init__(master, ctrl)
        self.header.config(text=u'[ %s ]' % _(u'Print RFID TAG'))

    def user_card_code_scan(self, scanned_code):
        if self.ctrl.card_code == scanned_code:
            log.debug(u'[current user is logging out]')
            self.ctrl.async(self.ctrl.view_mediator.logout_handler)
        else:
            log.debug(u'[a new user is logging in]')
            self.ctrl.card_code = scanned_code
            self.ctrl.async(self.ctrl.view_mediator.printrfid_login_handler)

    def order_code_scan(self, scanned_code):
        log.debug(u'[order scanned]')
        self.ctrl.order_code = scanned_code
        self.ctrl.async(self.ctrl.view_mediator.order_tag_check_handler)

    def process(self):
        if not any([
            self.ctrl.flags['confirming_order_tag_edit'],
            self.ctrl.flags['wait_for_peel_off'],
            self.ctrl.flags['block_input'],
        ]):
            log.debug(u'[RPi][ScanOrderCode][process code scanning]')
            scanned_code = self.code_var.get()
            self.code_var.set('')
            if unicode(scanned_code).startswith('vs00'):  # check if scanned code is a 'user card code'
                self.user_card_code_scan(scanned_code)
            else:  # the scanned code is a order code. normal scenario:
                self.order_code_scan(scanned_code)


class EditTag(Master):
    def __init__(self, master, ctrl):
        super(EditTag, self).__init__(master, ctrl)

        # TAG EDITION
        self.remove_tag_btn = RemoveTag(self)
        self.remove_tag_btn.place(x=5, y=10)
        self.label_tag = OrderTagCounterLabel(self)
        self.label_tag.place(x=134, y=25)
        label_tag_bottom = TagBottomLabel(self)
        label_tag_bottom.place(x=129, y=60)
        self.add_tag_btn = AddTag(self)
        self.add_tag_btn.place(x=213, y=10)

        # HANGER EDITION
        self.remove_hanger_btn = RemoveHanger(self)
        self.remove_hanger_btn.place(x=5, y=100)
        self.label_hanger = OrderTagCounterLabel(self)
        self.label_hanger.place(x=132, y=115)
        label_hanger_bottom = HangerBottomLabel(self)
        label_hanger_bottom.place(x=129, y=152)
        self.add_hanger_btn = AddHanger(self)
        self.add_hanger_btn.place(x=213, y=100)

    @property
    def item_count(self):
        return self.ctrl.item_count

    def handle_event(self, event):
        log.debug(u'[OrderTagEdit][Process][event:%s]' % event)

        if event == ADD_TAG_EVENT:
            self.add_tag()
        elif event == REMOVE_TAG_EVENT:
            self.remove_tag()
        elif event == ADD_HANGER_EVENT:
            self.add_hanger()
        elif event == REMOVE_HANGER_EVENT:
            self.remove_hanger()

    def add_tag(self):
        log.debug(u'[ADD TAG]')
        self.item_count.add_tag()
        self.label_tag.config(text=self.item_count.tags)
        self.remove_tag_btn.config(foreground='black')

    def remove_tag(self):
        log.debug(u'[REMOVE TAG]')
        if self.ctrl.data['order_tag'].tags_connected < self.item_count.tags:
            self.item_count.remove_tag()
            self.label_tag.config(text=self.item_count.tags)
        if self.item_count.tags == self.ctrl.data['order_tag'].tags_connected:
            self.remove_tag_btn.config(foreground='grey')

    def add_hanger(self):
        log.debug(u'[ADD HANGER]')
        self.item_count.hangers = int(self.item_count.hangers) + 1
        self.label_hanger.config(text=self.item_count.hangers)
        self.remove_hanger_btn.config(foreground='black')

    def remove_hanger(self):
        log.debug(u'[REMOVE HANGER]')
        if 0 < self.item_count.hangers:
            self.item_count.remove_hanger()
            self.label_hanger.config(text=self.item_count.hangers)
            if self.item_count.hangers == 0:
                self.remove_hanger_btn.config(fg='grey')

    def set_enter_btn_txt(self):
        order = self.ctrl.data['order_tag'].order_number
        btn_text = u'%s  %s' % (order, _(u'SAVE'))
        self.ctrl.gui_objects['order_tag_enter_button'].config(text=utils.crop_str(btn_text, 25))

    def reload(self):
        self.ctrl.flags['confirming_order_tag_edit'] = True
        self.label_tag.config(text=self.item_count.tags)
        hanger_btn_color = 'grey' if self.item_count.hangers == 0 else 'black'
        if self.item_count.tags == self.ctrl.data['order_tag'].tags_connected:
            self.remove_tag_btn.config(foreground='grey')
        self.remove_hanger_btn.config(foreground=hanger_btn_color)
        self.label_hanger.config(text=self.item_count.hangers)
        self.set_enter_btn_txt()


def main():
    try:
        app = PrintRFID()
        app.after(2000, app.run_form)  # show start frame delay
        app.after(1000, app.update_clock)  # start clock event_invoker
        app.mainloop()
    except Exception as e:
        log.debug(u'[PrintRFID][closed by exception! -> e:%s]' % unicode(e.message))
        raise


if __name__ == '__main__':
    main()
